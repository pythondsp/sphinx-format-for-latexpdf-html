.. _`file-num`:

Figures and listing
===================


* Following 'toctree' is used in this page. Look for the 'hidden' here. See details in :numref:`hiddentoctree`

.. code-block:: rst

    .. toctree::
        :maxdepth: 3
        :hidden:
        :caption: Contents:

        fig
        list




.. toctree::
    :maxdepth: 3
    :hidden:
    :caption: Contents:

    fig
    list
