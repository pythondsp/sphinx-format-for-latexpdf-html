
Figures
*******


Figure scaling and alignment
============================

* In HTML behavior of alignment dependents on the browser. Same happens in the LatexPDF. 
* Use center aligned or without delaring 'align' as it works well in both HTML and Latex. 
* Subfigures are not supported by Sphinx, but can be done using third part packages. 


**Code**

.. code-block:: rst

    .. figure:: figures/logo.jpg
        :scale: 20%
        :align: center

        Mastery in Servitude (center aligned)

    .. figure:: figures/logo.jpg
        :scale: 20%

        Mastery in Servitude (without align option)

**Result**

.. figure:: figures/logo.jpg
    :scale: 20%
    :align: center

    Mastery in Servitude (center aligned)

.. figure:: figures/logo.jpg
    :scale: 20%

    Mastery in Servitude (without align option)


