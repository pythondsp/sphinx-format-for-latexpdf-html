Listings
********

Number list
-----------

**Code**

.. code-block:: rst

    #. Data 1
    #. Data 2

         a. sub data 1
         b. sub data 2
            
    #. Data 3

         #) sub data 1
         #) sub data 2 
         #) sub data 3
            
**Result**

#. Data 1
#. Data 2

     a. sub data 1
     b. sub data 2
        
#. Data 3

     #) sub data 1
     #) sub data 2 
     #) sub data 3


Bullets
-------

**Code**

.. code-block:: rst

    * Data 1
    * Data 2
    * Data 3

        - sub data 1
        - sub data 2
        - sub data 3
          
**Result**

* Data 1
* Data 2
* Data 3

    - sub data 1
    - sub data 2
    - sub data 3

Mixed
-----

**Code**

.. code-block:: rst

    #. Data 1
        a. sub data 1
        b. sub data 2
    #. Data 2

        * sub data 1
        * sub data 2

**Result**

#. Data 1
    a. sub data 1
    b. sub data 2
#. Data 2

    * sub data 1
    * sub data 2
