.. _`BasicPattern`:

Basic Patterns
**************

.. raw:: latex

    \chapterquote{Who says God has created this world? We have created it by our own imagination.God is supreme, independent. When we say he has created this illusion, we lower him and his infinity. He is beyond all this.Only when we find him in ourselves, and even in our day to day life, do all doubts vanish.}{Meher Baba}


.. _`hiddentoctree`:

Toctree
=======

* Following code is added to index.rst. Look for the 'includehidden' here. It will show all the 'hidden contents' in the TOC. See next point for clarification. 

.. code-block:: rst

    .. toctree::
        :maxdepth: 3
        :numbered:
        :includehidden:
        :caption: Contents:

        sphinx_basics/readme
        sphinx_basics/basics
        num-fig/numfig_index

* Above toctree contins 'fig-num/numfig_index'; and the :ref:`fig-num/numfig_index.rst<file-num>` file contains following contents in it. 


    .. code-block:: rst

        .. toctree::
            :maxdepth: 3
            :hidden:
            :caption: Contents:

            fig
            list

* Note that 'hidden' is used in 'numfig_index.rst'. Therefore, it will not be dislayed in the 'html' page of 'num-fig/numfig_index'. 
* But, includehidden is used in the index.rst, therefore the 'toctree of numfig_index.rst' will be added in the top level 'table of contents'. 
* Also, Latex will include only top level 'table of contents' in all cases. 


Quotes
======

Above the chapter name in Latex
-------------------------------

Use below code and the quote will appear in Latex file only. 

**Code**

.. code-block:: rst

    .. raw:: latex

        \chapterquote{Who says God has created this world? We have created it by 
        our own imagination.God is supreme, independent. When we say he has 
        created this illusion, we lower him and his infinity. He is beyond all 
        this.Only when we find him in ourselves, and even in our day to day life, 
        do all doubts vanish.}{Meher Baba}


HTML and Latex
--------------

Use epigraph for quotes in HTML and Latex files as below, 

**Code**

.. code-block:: rst

    .. epigraph:: 
        
        Who says God has created this world? We have created it by our own 
        imagination.God is supreme, independent. When we say he has created 
        this illusion, we lower him and his infinity. He is beyond all this.
        Only when we find him in ourselves, and even in our day to day life, 
        do all doubts vanish.

        -- Meher Baba


**Result**

.. epigraph:: 
    
    Who says God has created this world? We have created it by our own 
    imagination.God is supreme, independent. When we say he has created 
    this illusion, we lower him and his infinity. He is beyond all this.
    Only when we find him in ourselves, and even in our day to day life, 
    do all doubts vanish.

        -- Meher Baba


Only HTML
---------

Below quote will appear in HTML only, 

**Code**

.. code-block:: rst

    .. only:: html

        .. epigraph:: 
            
            Who says God has created this world? We have created it by our own 
            imagination.God is supreme, independent. When we say he has created 
            this illusion, we lower him and his infinity. He is beyond all this.
            Only when we find him in ourselves, and even in our day to day life, 
            do all doubts vanish.

                -- Meher Baba

**Result**

.. only:: html
    
    .. epigraph:: 
        
        Who says God has created this world? We have created it by our own 
        imagination.God is supreme, independent. When we say he has created 
        this illusion, we lower him and his infinity. He is beyond all this.
        Only when we find him in ourselves, and even in our day to day life, 
        do all doubts vanish.

            -- Meher Baba

Admonitions
===========

Admonitions can be used for custom headings for notes and warnigs etc. Following are the possible classeds for it, 

* attention, 
* caution 
* danger 
* error 
* hint 
* important 
* note 
* tip 
* warning


.. code-block:: text

    .. admonition:: Codes and Datasets
       :class: danger

       Below is x = 2, 

       .. code-block:: c
          
        int x = 2;



.. admonition:: Codes and Datasets
    :class: danger

    Below is x = 2, 

    .. code-block:: c
          
        int x = 2;

